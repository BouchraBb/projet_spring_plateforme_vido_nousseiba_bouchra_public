<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html html xmlns="http://www.w3.org/1999/xhtml"
            xmlns:th="http://www.thymeleaf.org">>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<link rel="stylesheet" href="style.css">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link
	href="https://fonts.googleapis.com/css2?family=Abril+Fatface&display=swap"
	rel="stylesheet">
<body>
	<h2>
		<c:out value="formulaire d'inscription d'abonne" />
	</h2>
	<div class="container">
		<form action="/inscription" th:object="${utilisateur}"  method="post">

			<div class="row">
				<div class="col-25">
					<LABEL for="nom"><c:out value="Nom * " /> </LABEL>
				</div>
				<div class="col-75">
					<input type="text" name="nom" th:field="*{nom}" required>
				</div>
			</div>

			<div class="row">
				<div class="col-25">
					<LABEL for="prenom"><c:out value="Prénom " /> </LABEL>
				</div>
				<div class="col-75">
					<input type="text" name="prenom" th:field="*{prenom}" >
				</div>
			</div>
				<div class="row">
            				<div class="col-25">
            					<LABEL for="pseudo"><c:out value="Pseudo " /> </LABEL>
            				</div>
            				<div class="col-75">
            					<input type="text" name="pseudo" th:field="*{pseudo}" >
            				</div>
            			</div>

			<div class="row">
				<div class="col-25">
					<LABEL for="email"><c:out value="Adresse email" /> </LABEL>
				</div>
				<div class="col-75">
					<input type="text" name="email"  th:field="*{email}" required>
				</div>
			</div>

			<div class="row">
                <div class="col-25">
                    <LABEL for="motDePasse"><c:out value="Definir un mot de pass" /> </LABEL>
                </div>
                <div class="col-75">
                    <input type="text" name="motDePasse"  th:field="*{motDePasse}"  required>
                </div>
            </div>

            <div class="row">
                <div class="col-25">
                    <LABEL for="motdePasse1"><c:out value="Réecrire le mot de pass" /> </LABEL>
                </div>
                <div class="col-75">
                    <input type="text" name="motdePasse1" required >
                </div>
            </div>

			<br>
			<div class="caption">
				<INPUT type="submit" value="Envoyer ma demande">
			</div>
		</form>
	</div>

</body>
</html>