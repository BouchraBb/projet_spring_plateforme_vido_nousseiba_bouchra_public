<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.io.ByteArrayInputStream" %>
<%@ page import="java.io.InputStream" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<title>Ma vidéo</title>
  <script src="/script/reprendreVideo.js"></script>
<link rel="stylesheet" href="../style.css">
<link rel="stylesheet" href="../commentaires.css">
     <link rel="stylesheet"
     	href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
     	<link rel="stylesheet"
        	href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
        	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        	crossorigin="anonymous">
	<meta charset="UTF-8">



</head>
<body>

<div>
   <video id="my-video"  data-timepause="${timePause}" controls>
           	<source id="resume-button"  src="data:video/mp4;base64,${Base64.encodeBase64String(videoALire)}"  type="video/mp4" >

     </video>


</div>
    <p id="myP"> pause :  </p>
<div>
    <c:out value="nombre de vues : ${video.vues} vues"/>
</div>
            <div class="col-lg-4 col-md-4 col-sm-4 offset-md-1 offset-sm-1 col-12 mt-4">
                <form id="algin-form" method="post" action="/ajouterCommentaire/${video.idVideo}">
                    <div class="form-group">
                        <h4>Laisser un commentaire</h4>
                        <label for="message">Message</label>
                        <textarea name="message" id=""msg cols="30" rows="5" class="form-control" style="background-color: black;"></textarea>
                    </div>
     <div class="form-group">
      <label for="score">Score</label>
      <select name="score" id="score">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>

      </select>
        </div>
                    <div class="form-group">
                        <button type="submit" id="post" class="btn">Poster le Commentaire</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

     </body>
     </html>
