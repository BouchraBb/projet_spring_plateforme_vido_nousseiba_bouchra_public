<%@ page import="org.apache.commons.codec.binary.Base64"%>
<%@ include file="../components/head.jsp"%>
<title>utilisateurPage</title>
<link rel="stylesheet" href="../style.css">
</head>

<body>


	<div>
		<h4>mes favoris </h4>

			<c:forEach var="favorit" items="${favoritList}" varStatus="status">
				<div class="parent container" style="background-color: rgb(28, 31, 17);">
				<div>
					<div class="card" style="">
						<div>
							<div class="video-thumbnail">
								<img width="320"
									src="data:image/png;base64,${Base64.encodeBase64String(favorit.byteMiniature)}"
									alt="miniature"> <a href="/lireVideo/${favorit.idVideo}"
									class="play-button"><i class="bi bi-play-circle"
									style="color: red; font-size: 50px;"></i></a>
							</div>
						</div>
						<div class="card-body">
							<h5 class="card-title">${historique.video.titre}</h5>
							<p class="card-text">${historique.dateHistorique}</p>

							<a class="btn btn-primary" href="/detailsvideo/${favorit.idVideo}">
								afficher les details</a>
								<a id="link-req-${favorit.idVideo}" href="#" onClick="defav('${favorit.idVideo}')" >
                                        <i id="fav-icon-${favorit.idVideo}" class="bi bi-heart-fill"
                                        style="color: red; font-size: 40px;"></i>
                                </a>
							</td>
						</div>
					</div>
				</div>
		</div>

		</c:forEach>
	</div>

	</div>
	<script>
           function defav(videoId){
                var link =  document.getElementById("link-req-" + videoId);
                link.setAttribute("href","/fav/deleteFavorit/"+videoId);

           }
           </script>
</body>
</html>
