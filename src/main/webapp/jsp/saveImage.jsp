<!DOCTYPE html>
   <head>
   <link rel="stylesheet" href="style.css">
      <meta charset="UTF-8">
      <title>Upload Image</title>
   </head>
   <body>
      <form method="post" action="/upload/image" enctype="multipart/form-data">
         <input type="file" name="file">
         <button type="submit">Upload</button>
      </form>
   </body>
</html>
