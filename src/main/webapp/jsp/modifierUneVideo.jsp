<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html html xmlns="http://www.w3.org/1999/xhtml"
            xmlns:th="http://www.thymeleaf.org">
<head>
<meta charset="ISO-8859-1">
<title>Modifier une video</title>
</head>

<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link
	href="https://fonts.googleapis.com/css2?family=Abril+Fatface&display=swap"
	rel="stylesheet">
   <body>


<link rel="stylesheet" href="../style.css">

      <h2>Modification d'une video</h2>
	<div class="container">
	<form action="/updateOneVideo/${video.idVideo}" th:object="${video}"  method="post" enctype="multipart/form-data">

        <div class="row">
        <LABEL for="miniature"><c:out value="Miniature :" /> </LABEL>
        <input type="file" name="miniature">
        </div>
			<div class="row">
				<div class="col-25">
					<LABEL for="titre"><c:out value="Titre :" /> </LABEL>
				</div>
				<div class="col-75">
					<input type="text" name="titre" th:field="*{titre}" required value="<c:out value="${video.titre}"/>">                                                                          						value="<c:out value="${pokemon.nom}"/>">>
				</div>
			</div>

			<div class="row">
				<div class="col-25">
					<LABEL for="duree"><c:out value="Duree  (en min):" /> </LABEL>
				</div>
				<div class="col-75">
					<input type="text" name="duree" th:field="*{duree}" required value="<c:out value="${video.duree}"/>">
				</div>
			</div>
				<div class="row">
            				<div class="col-25">
            					<LABEL for="genre"><c:out value="Genre :" /> </LABEL>
            				</div>
            				<div class="col-75">
<select name="genre" th:field="*{genre}" required value="<c:out value="${video.genre}"/>">  >
   <option value="action">Action</option>
   <option value="animation">Animation</option>
   <option value="aventure">Aventure</option>
   <option value="biographique">Biographique</option>
   <option value="comedie">Comédie</option>
   <option value="documentaire">Documentaire</option>
   <option value="drame">Drame</option>
   <option value="fantastique">Fantastique</option>
   <option value="guerre">Guerre</option>
   <option value="historique">Historique</option>
   <option value="horreur">Horreur</option>
   <option value="musical">Musical</option>
   <option value="policier">Policier</option>
   <option value="romance">Romance</option>
   <option value="science-fiction">Science-fiction</option>
   <option value="thriller">Thriller</option>
   <option value="western">Western</option>
      <option value="court metrage">Court métrage</option>
</select>
</div>
</div>

			<div class="row">
				<div class="col-25">
					<LABEL for="description"><c:out value="Description :" /> </LABEL>
				</div>
				<div class="col-75">
					<input type="text" name="description"  th:field="*{description}" required value="<c:out value="${video.description}"/>">  >
				</div>
			</div>

			<div class="row">
                <div class="col-25">
                    <LABEL for="date"><c:out value="Date :" /> </LABEL>
                </div>
                <div class="col-75">
                    <input type="date" name="annee"  th:field="*{annee}"  required value="<c:out value="${video.annee}"/>">  >
                </div>
            </div>


			<br>
			<div class="caption">
				<INPUT type="submit" value="Envoyer">
			</div>
		</form>
	</div>




   </body>
</html>
