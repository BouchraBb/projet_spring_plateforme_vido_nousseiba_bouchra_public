<%@ page import="org.apache.commons.codec.binary.Base64"%>
<%@ include file="../components/head.jsp"%>
<title>utilisateurPage</title>
</head>
<link rel="stylesheet" href="style.css">
<body>


	<div>
		<h4>mon historique</h4>

			<c:forEach var="historique" items="${historicList}" varStatus="status">
				<div class="parent container" style="background-color: rgb(28, 31, 17);">
				<div>
					<div class="card" style="">
						<div>
							<div class="video-thumbnail">
								<img width="320"
									src="data:image/png;base64,${Base64.encodeBase64String(historique.video.byteMiniature)}"
									alt="miniature"> <a href="/lireVideo/${video.idVideo}"
									class="play-button"><i class="bi bi-play-circle"
									style="color: red; font-size: 50px;"></i></a>
							</div>
						</div>
						<div class="card-body">
							<h5 class="card-title">${historique.video.titre}</h5>
							<p class="card-text">${historique.dateHistorique}</p>

							<a class="btn btn-primary" href="/detailsvideo/${video.idVideo}">
								afficher les details</a>
							</td>
						</div>
					</div>
				</div>
		</div>

		</c:forEach>
	</div>

	</div>
</body>
</html>
