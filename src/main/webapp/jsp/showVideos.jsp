<%@ page import="org.apache.commons.codec.binary.Base64"%>
<%@ include file="../components/head.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<link rel="stylesheet" href="style.css">
<title>utilisateurPage</title>
</head>

<body>
	<div>
		<%@ include file="../components/navbar.jsp"%>
	</div>
	<div class="space">
		<div>
			<input type="text" name="search" style="width: 90%;"> <a
				href="#lien recherche "><i class="bi bi-search"></i></a>
		</div>

		<div class="space1">
			<div>
				<a href="/showHistoric" style="width: 30%;"><i
					class="bi bi-clock-history" style="font-size: 30px;"></i></a> &ensp;
			</div>
			<div>
				<a href="/showFavorit" style="width: 30%;"><i class="bi bi-list-stars"
					style="font-size: 30px;"></i></a> &ensp;
			</div>
			<div>
				<a href="#" style="width: 30%;"><i class="bi bi-person"
					style="font-size: 30px;"></i></a>
				<c:out value="${utilisateur.pseudo}" />
				&ensp;
			</div>
		</div>
	</div>

	<div>
		<h4>Suggestions de videos Populaires (+ de vues, score</h4>
	</div>

	<div>
		<h4>Suggestions de videos Recommande pour user (mot cle)</h4>
	</div>

	<div>
		<h4>Reprendre la lecture de videos du user</h4>
	</div>

	<div>
		<h4>Suggestions d autres videos (genre)</h4>
		<c:set var="favorits" value="${favoritList}" />


		<div class="parent container">
			<c:forEach var="video" items="${listVideos}" varStatus="status">
				<div>

					<div class="card" style="">
						<div>

							<div class="video-thumbnail">

								<img width="320"
									src="data:image/png;base64,${Base64.encodeBase64String(video.byteMiniature)}"
									alt="miniature"> <a href="/lireVideo/${video.idVideo}"
									class="play-button"><i class="bi bi-play-circle"
									style="color: red; font-size: 50px;"></i></a>

							</div>
						</div>
						<div class="card-body">
							<h5 class="card-title">${video.titre}</h5>
							<p class="card-text">${video.annee}- ${video.genre} -
								${video.duree} mn</p>
							<p class="card-text"></p>
							<a class="btn btn-primary" href="/detailsvideo/${video.idVideo}">
								afficher les details</a>

							<a id="link-req-${video.idVideo}" href="#" onClick="fav('${video.idVideo}')" >
                               <c:choose>
                                        <c:when test="${fn:contains(favorits, video)}">
                                            <i id="fav-icon-${video.idVideo}" class="bi bi-heart-fill"
                                            style="color: red; font-size: 40px;"></i>
                                        </c:when>

                                        <c:otherwise>
                                             <i id="fav-icon-${video.idVideo}" class="bi bi-heart"
                                                       style="color: red; font-size: 40px;"></i>
                                        </c:otherwise>
                                </c:choose>

                            </a>

						</div>

					</div>


				</div>

			</c:forEach>
		</div>

	</div>



	<script>
       function fav(videoId){
            var icon = document.getElementById("fav-icon-" + videoId);
            var iconClass = icon.getAttribute("class");
            var link =  document.getElementById("link-req-" + videoId);

            if(iconClass ==="bi bi-heart"){
                icon.setAttribute("class", "bi bi-heart-fill");
                link.setAttribute("href","/addFavorit/"+videoId);

                }else if(iconClass ==="bi bi-heart-fill"){
                    icon.setAttribute("class", "bi bi-heart");
                    link.setAttribute("href","/deleteFavorit/"+videoId);
                 }
       }
       </script>

</body>


</html>