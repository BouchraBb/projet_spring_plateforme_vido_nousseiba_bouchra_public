<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.io.ByteArrayInputStream" %>
<%@ page import="java.io.InputStream" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="style.css">
	<meta charset="UTF-8">
	<title>Ma vidéo</title>
</head>
<body>




<c:forEach var="video" items="${videoData}" varStatus="status">
     <video width="320" height="240" controls>

           	<source src="data:video/mp4;base64,${Base64.encodeBase64String(video)}" type="video/mp4">
           </video>
</c:forEach>



</body>
</html>
