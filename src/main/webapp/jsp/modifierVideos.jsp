<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.io.ByteArrayInputStream" %>
<%@ page import="java.io.InputStream" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>

     <link rel="stylesheet"
     	href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
     	<link rel="stylesheet"
        	href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
        	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        	crossorigin="anonymous">
        	<link rel="stylesheet" href="../style.css">
	<meta charset="UTF-8">
	<title>Modifier les vidéos</title>
</head>
<body>




<div class="parent container">
    <c:forEach var="video" items="${videoData}" varStatus="status">
<div >

    <div class="card" style="">
    <div >

        <div class="video-thumbnail">

                 <img width="320" src="data:image/png;base64,${Base64.encodeBase64String(video.byteMiniature)}" alt="miniature">

          <a href="/lireVideo/${video.idVideo}" class="play-button"><i class="bi bi-play-circle" style="color:red ; font-size:50px; "></i></a>

        </div>
      </div>
      <div class="card-body">
        <h5 class="card-title">${video.titre}</h5>
        <p class="card-text">${video.annee} - ${video.genre} - ${video.duree} mn </p>
        <p class="card-text"></p>

        <a class="btn btn-primary" href="/detailsvideo/${video.idVideo}"> afficher les détails</a></td>
        <a class="btn btn-primary" href="/updateOneVideo/${video.idVideo}"> Modifier</a></td>
        <a class="btn btn-primary" href="/admin/deleteVideo/${video.idVideo}"> Supprimer</a></td>
      </div>
    </div>
    </div>

        </c:forEach>
        </div>

    </div>





</body>
</html>
