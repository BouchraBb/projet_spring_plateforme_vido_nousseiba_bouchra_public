<%@ page import="org.apache.commons.codec.binary.Base64" %>


<%@ include file="../components/head.jsp" %>
<title></title>
</head>
<link rel="stylesheet" href="style.css">

<body>
     <div>
    <%@ include file="../components/navbar.jsp" %>
      </div>

    <h1>Détails de la video</h1>





<div class="video-thumbnail">

         <img width="320" src="data:image/png;base64,${Base64.encodeBase64String(video.byteMiniature)}" alt="miniature">

  <a href="/lireVideo/${video.idVideo}" class="play-button"><i class="bi bi-play-circle" style="color:red ; font-size:50px; "></i></a>

</div>


    <h2>ajouter aux favoris</h2>

<a href="/listedelecture/ajouter/${video.idVideo}" ><i class="bi bi-plus-square"></i></a>

<a href="/listedelecture/retirer/${video.idVideo}" ><i class="bi bi-dash-square"></i></a>


 <h2>Score de la video</h2>
 <a href="/scoreVideo/${video.idVideo}" >afficher le score de la video</a></br>
    <c:out value="score moyen  : ${scoreVideo}"/>


    <h2>Description</h2>
    <c:out value="titre : ${video.titre}"/>
    </br>
    <c:out value="duree : ${video.duree} minutes"/>
    <br>
    <c:out value="genre : ${video.genre}"/>
     <br>
    <c:out value="${descriptionVideo}"/>

     <h2>Mot cles</h2>
       <c:forEach items="${video.motCles}" var="mot">
        <c:out value="${mot.libelle}"/>
         </c:forEach>

    <h2>Images</h2>
    <div>
      <a href="/image/show/allImagesByVideo/${video.idVideo}"> afficher toutes les images de la video</a></li>
</div>


    <c:forEach var="image" items="${imageData2}" varStatus="status">
         <img width="320"

                src="data:image/png;base64,${Base64.encodeBase64String(image)}" type="image/png">

    </c:forEach>



    <h2>Critiques
    <a  href="/showCritiquesByVideo/${video.idVideo}"> <i class="bi bi-caret-down"></i></a></li>
    <a  href="/hideCritiquesByVideo/${video.idVideo}"> <i class="bi bi-caret-up"></i></a></li>
    </h2>



<div class="parent container" id="critiques">
    <c:forEach items="${critiquesVideo}" var="critique">
<div class="">

    <div class="card" style="">
      <div class="card-body">
         <p class="card-text">utilisateur : ${critique.utilisateur.nom}</p>
           <p class="card-text">date : ${critique.dateCritique}</p>
        <h5 class="card-title">${critique.commentaire}</h5>

        <p class="card-text">score : ${critique.scoreToStars(critique.score)}</p>


      </div>
    </div>
    </div>

        </c:forEach>
        </div>


    <h2>Suggestions de vidéos similaires</h2>



<div class="parent container">
    <c:forEach var="video" items="${listVideos}" varStatus="status">
<div >

    <div class="card" style="">
    <div >

        <div class="video-thumbnail">

                 <img width="320" src="data:image/png;base64,${Base64.encodeBase64String(video.byteMiniature)}" alt="miniature">

          <a href="/lireVideo/${video.idVideo}" class="play-button"><i class="bi bi-play-circle" style="color:red ; font-size:50px; "></i></a>

        </div>
      </div>
      <div class="card-body">
        <h5 class="card-title">${video.titre}</h5>
        <p class="card-text">${video.annee} - ${video.genre} - ${video.duree} mn </p>
        <p class="card-text"></p>

        <a class="btn btn-primary" href="/detailsvideo/${video.idVideo}"> afficher les détails</a></td>
      </div>
    </div>
    </div>

        </c:forEach>
        </div>

    </div>








</body>


</html>