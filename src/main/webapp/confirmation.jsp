<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
<link rel="stylesheet" href="./style.css">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link
	href="https://fonts.googleapis.com/css2?family=Abril+Fatface&display=swap"
	rel="stylesheet">
<head>
<meta charset="ISO-8859-1">
<title><c:out value="bienvenue" /></title>
</head>
<body>
	<c:set scope="request" var="u" value="${ utilisateur }" />
	<h6>
		<c:out value="Bonjour " />
		<c:out value=" ${u.pseudo}" />
		<c:out value=", bienvenue sur notre plateforme video  " />
	</h6>

</body>
</html>