package fr.afpa.plateformevideo.repository;

import fr.afpa.plateformevideo.entity.Acteur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActeurRepository extends JpaRepository<Acteur, Integer> {
}
