package fr.afpa.plateformevideo.repository;

import fr.afpa.plateformevideo.entity.Critique;
import fr.afpa.plateformevideo.entity.Utilisateur;
import fr.afpa.plateformevideo.service.CritiqueService;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.persistence.NamedQuery;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Repository
public interface CritiqueRepository extends JpaRepository<Critique, Integer> {

    public List<Critique> critiquesByVideo(int idVideo) ;

    @Query("select AVG(a.score) from Critique a where a.video.idVideo = ?1")
    public Double scoreByVideo(int idVideo) ;




}
