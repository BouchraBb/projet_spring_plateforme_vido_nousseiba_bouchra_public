package fr.afpa.plateformevideo.repository;

import fr.afpa.plateformevideo.entity.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UtilisateurRepository extends JpaRepository<Utilisateur, Integer>   {


    public Utilisateur findByEmailAndPassword(String email, String password);
}
