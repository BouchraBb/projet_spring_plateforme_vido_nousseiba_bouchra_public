package fr.afpa.plateformevideo.repository;

import fr.afpa.plateformevideo.entity.Critique;
import fr.afpa.plateformevideo.entity.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface ImageRepository extends JpaRepository<Image, Integer> {

    public Image miniatureVideo(int idVideo) ;
}
