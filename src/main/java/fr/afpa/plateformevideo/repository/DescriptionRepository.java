package fr.afpa.plateformevideo.repository;

import fr.afpa.plateformevideo.entity.Description;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DescriptionRepository extends JpaRepository<Description, Integer> {
}
