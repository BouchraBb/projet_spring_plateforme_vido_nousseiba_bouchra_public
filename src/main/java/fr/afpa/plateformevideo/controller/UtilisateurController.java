package fr.afpa.plateformevideo.controller;

import fr.afpa.plateformevideo.dto.UtilisateurDTO;
import fr.afpa.plateformevideo.entity.Historique;
import fr.afpa.plateformevideo.entity.Utilisateur;
import fr.afpa.plateformevideo.entity.Video;
import fr.afpa.plateformevideo.service.HistoriqueService;
import fr.afpa.plateformevideo.service.ImageService;
import fr.afpa.plateformevideo.service.UtilisateurService;
import fr.afpa.plateformevideo.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
public class UtilisateurController {
    @Autowired
    UtilisateurService utilisateurService;
    @Autowired
    VideoService videoService;

    @PostMapping("/inscription")
    public String inscription(Model model, @ModelAttribute("utilisateur") Utilisateur utilisateur, BindingResult bindingResult ){
        if (bindingResult.hasErrors()) {
            return "formulaire_inscription";
        }
        UtilisateurDTO user =utilisateurService.create(utilisateur);
        model.addAttribute("utilisateur", user);
        return "confirmation";
    }

    @PostMapping("/connexion")
    public String connexion(HttpServletRequest request, @RequestParam("email") String email, @RequestParam("motDePasse") String motDePasse){
        UtilisateurDTO utilisateur =  utilisateurService
                .authentificate(email,motDePasse);
        if(utilisateur!=null) {
            request.getSession().setAttribute("utilisateur", utilisateur);
           uploadAllList(request);
            return "jsp/showVideos";
        }
        else return "formulaire_connexion";
    }




    @RequestMapping(value = "/addFavorit/{idVideo}", method = RequestMethod.GET)
    public String addFavorit(Model model,HttpServletRequest request, @PathVariable ("idVideo") Integer id) {
        Video video=videoService.findById(id);
        UtilisateurDTO user =(UtilisateurDTO) request.getSession().getAttribute("utilisateur");
        utilisateurService.addVideoToFavorit(user,id );
        uploadAllList(request);
        return "jsp/showVideos";
    }

    @RequestMapping(value = "/deleteFavorit/{idVideo}", method = RequestMethod.GET)
    public String deleteFavorit(Model model,HttpServletRequest request, @PathVariable ("idVideo") Integer id) {
        Video video=videoService.findById(id);
        UtilisateurDTO user =(UtilisateurDTO) request.getSession().getAttribute("utilisateur");
        utilisateurService.removeVideoFromFavorit(user,id );
        uploadAllList(request);
        return "jsp/showVideos";
    }


    @RequestMapping("/showFavorit")
    public String showFavorit(HttpServletRequest request) {
        List<Video> favoritList = utilisateurService.userFavorit(((UtilisateurDTO)request.getSession().getAttribute("utilisateur")).getIdUtilisateur());
        request.getSession().setAttribute("favoritList", favoritList);
        return "jsp/favorit";
    }


    private void uploadAllList(HttpServletRequest request) {
        List<Video> listVideos = videoService.uploadMiniature(videoService.findAll());
        request.getSession().setAttribute("listVideos", listVideos);

        List<Video> favoritList = utilisateurService.userFavorit(((UtilisateurDTO)request.getSession().getAttribute("utilisateur")).getIdUtilisateur());
        request.getSession().setAttribute("favoritList", favoritList);
    }


    @RequestMapping(value = "/fav/deleteFavorit/{idVideo}", method = RequestMethod.GET)
    public String deleteFromFavorit(Model model,HttpServletRequest request, @PathVariable ("idVideo") Integer id) {
        Video video=videoService.findById(id);
        UtilisateurDTO user =(UtilisateurDTO) request.getSession().getAttribute("utilisateur");
        utilisateurService.removeVideoFromFavorit(user,id );
        uploadAllList(request);
        return "jsp/favorit";
    }

}
