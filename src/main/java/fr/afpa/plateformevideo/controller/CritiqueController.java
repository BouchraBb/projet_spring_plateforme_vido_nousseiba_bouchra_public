package fr.afpa.plateformevideo.controller;

import fr.afpa.plateformevideo.dto.UtilisateurDTO;
import fr.afpa.plateformevideo.entity.Critique;
import fr.afpa.plateformevideo.entity.Utilisateur;
import fr.afpa.plateformevideo.entity.Video;
import fr.afpa.plateformevideo.service.CritiqueService;
import fr.afpa.plateformevideo.service.ImageService;
import fr.afpa.plateformevideo.service.UtilisateurService;
import fr.afpa.plateformevideo.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@Controller
public class CritiqueController {

    @Autowired
    CritiqueService critiqueService;

    @Autowired
    VideoService videoService;

    @Autowired
    UtilisateurService utilisateurService;


    @RequestMapping("/showCritiquesByVideo/{idVideo}")
    public String getCritiquesByVideo(Model model, HttpServletRequest request, @PathVariable("idVideo") Integer id ) {
        List<Critique> critiquesVideo = critiqueService.getCritiquesByVideo(id);
        model.addAttribute("critiquesVideo", critiquesVideo);

        return "jsp/detailsvideo";

    }

    @RequestMapping("/hideCritiquesByVideo/{idVideo}")
    public String hideCritiquesByVideo(Model model, HttpServletRequest request, @PathVariable("idVideo") Integer id ) {
        model.addAttribute("critiquesVideo", null);

        return "jsp/detailsvideo";

    }

    @PostMapping("/ajouterCommentaire/{idVideo}")
    public String ajouterCommentaireVideo(Model model, HttpServletRequest request, @PathVariable("idVideo") Integer id, @RequestParam("message") String message,@RequestParam("score") String score) {

        UtilisateurDTO utilisateur=(UtilisateurDTO) request.getSession().getAttribute("utilisateur");
        Utilisateur user=utilisateurService.findById1(utilisateur.getIdUtilisateur());
        Critique critique=new Critique(message, Date.valueOf(LocalDate.now()), Integer.parseInt(score),user ,videoService.findById(id));
        critiqueService.create(critique);

        return "jsp/lirevideo";

    }


}
