package fr.afpa.plateformevideo.controller;


import fr.afpa.plateformevideo.dto.UtilisateurDTO;
import fr.afpa.plateformevideo.entity.*;
import fr.afpa.plateformevideo.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class VideoController {

    @Autowired
    VideoService videoService;
    @Autowired
    ImageService imageService;
    @Autowired
    CritiqueService critiqueService;
    @Autowired
    UtilisateurService utilisateurService;
    @Autowired
    LectureService lectureService;

    @RequestMapping(value = "/upload/video", method = RequestMethod.GET)
    public String uploadVideo() {
        return "jsp/saveVideo";
    }

    @RequestMapping(value = "/detailsvideo/{idVideo}", method = RequestMethod.GET)
    public String getDetailsVideo(HttpServletRequest request, @PathVariable("idVideo") Integer id) {
        request.getSession().setAttribute("imageData2", null);
        request.getSession().setAttribute("miniature", null);


        Video video = videoService.findById(id);
        request.getSession().setAttribute("video", video);
        request.getSession().setAttribute("descriptionVideo", video.getDescription());

        byte[] miniature = imageService.showMiniatureByIdVideo(id);
        request.getSession().setAttribute("miniature", miniature);

        byte[][] images = imageService.showAllImagesByVideo(id);
        request.getSession().setAttribute("imageData2", images);


        List<Video> listVideos = videoService.videosSimilaires(id);
/*        byte[][] miniatures=new byte[listVideos.size()][];
        for (int i=0; i<listVideos.size();i++){
            miniatures[i]= imageService.showMiniatureByIdVideo(listVideos.get(i).getIdVideo());
            listVideos.get(i).setByteMiniature(miniatures[i]);
            videoService.update(listVideos.get(i));
        }*/

        request.getSession().setAttribute("listVideos", listVideos);



/*        List<Critique> critiquesVideo = critiqueService.getCritiquesByVideo(id);
        request.getSession().setAttribute("critiquesVideo", critiquesVideo);*/

        return "jsp/detailsvideo";
    }

    @RequestMapping(value = "/lireVideo/{idVideo}", method = RequestMethod.GET)
    public String lireVideo(Model model, HttpServletRequest request, @PathVariable("idVideo") Integer id) {
        Video video = videoService.findById(id);
        UtilisateurDTO user = (UtilisateurDTO) request.getSession().getAttribute("utilisateur");

        utilisateurService.addVideoToHistorique(user, id);

        lectureService.addVideoToLecture(user, id);

        byte[] videoByte = videoService.showOneVideo(video);
        request.getSession().setAttribute("videoALire", videoByte);
        video.ajouterUneVue();

        Lecture lecture = lectureService.userVideoLecture(user.getIdUtilisateur(), id);
        request.getSession().setAttribute("timePause", lecture.getTimePause());

        videoService.update(video);
        request.getSession().setAttribute("video", video);
        return "jsp/lirevideo";
    }


    @PostMapping("/upload/video")
    public String uploadVideo(@RequestParam("file") MultipartFile file, @RequestParam("miniature") MultipartFile miniature, @ModelAttribute("video") Video video, BindingResult result) {

        if (!result.hasErrors()) {
            String name = videoService.uploadVideoIntoBucket(file);
            video.setTitre(name);

            Image min = imageService.uploadImage(miniature);
            min.setMiniature(true);

            video.getImages().add(min);

            Video video2 = videoService.create(video);
            min.setVideo(video2);
            imageService.update(min);

            byte[] minByte = imageService.showMiniatureByIdVideo(video2.getIdVideo());
            video2.setByteMiniature(minByte);
            videoService.update(video2);
            //videoService.update(vid);
        }

        return "jsp/saveVideo";
    }


    @RequestMapping(value = "/video/show/all", method = RequestMethod.GET)
    public ModelAndView showAll() throws Exception {
        byte[][] videoList = videoService.showVideos(videoService.findAll());
        ModelAndView mav = new ModelAndView("jsp/videos"); // créez un objet ModelAndView avec la vue JSP nommée "video"
        mav.addObject("videoData", videoList); // ajoutez les données de la vidéo à l'objet ModelAndView

        return mav; // retournez l'objet ModelAndView à la vue JSP
    }

    @RequestMapping(value = "/listedelecture/ajouter/{idVideo}", method = RequestMethod.GET)
    public String addVideoToFavorit(HttpServletRequest request, @PathVariable("idVideo") Integer id) {
        utilisateurService.addVideoToFavorit((UtilisateurDTO) request.getSession().getAttribute("utilisateur"), id);
        return "jsp/detailsvideo";
    }

    @RequestMapping(value = "/listedelecture/retirer/{idVideo}", method = RequestMethod.GET)
    public String removeVideoFromFavorit(HttpServletRequest request, @PathVariable("idVideo") Integer id) {
        utilisateurService.removeVideoFromFavorit((UtilisateurDTO) request.getSession().getAttribute("utilisateur"), id);
        return "jsp/detailsvideo";
    }

    @RequestMapping(value = "/scoreVideo/{idVideo}", method = RequestMethod.GET)
    public String scoreVideo(HttpServletRequest request, @PathVariable("idVideo") Integer id) {

        Double scoreVideo = critiqueService.scoreByVideo(id);
        request.getSession().setAttribute("scoreVideo", scoreVideo);

        return "jsp/detailsvideo";
    }


    @RequestMapping(value = "/admin/updateVideos", method = RequestMethod.GET)
    public ModelAndView updateVideos() throws Exception {
        List<Video> listVideos = videoService.findAll();
        //byte[][] videoList=videoService.showVideos(listVideos);
        byte[][] miniatures = new byte[listVideos.size()][];
        for (int i = 0; i < listVideos.size(); i++) {
            miniatures[i] = imageService.showMiniatureByIdVideo(listVideos.get(i).getIdVideo());
            listVideos.get(i).setByteMiniature(miniatures[i]);
            videoService.update(listVideos.get(i));
        }

        ModelAndView mav = new ModelAndView("jsp/modifierVideos"); // créez un objet ModelAndView avec la vue JSP nommée "video"
        mav.addObject("videoData", listVideos); // ajoutez les données de la vidéo à l'objet ModelAndView

        return mav; // retournez l'objet ModelAndView à la vue JSP
    }

    @RequestMapping(value = "/updateOneVideo/{idVideo}", method = RequestMethod.GET)
    public String updateOneVideo(HttpServletRequest request, @PathVariable("idVideo") Integer id, Model model) {
        Video video = videoService.findById(id);
        model.addAttribute("video", video);
        return "jsp/modifierUneVideo";
    }


    @PostMapping("/updateOneVideo/{idVideo}")
    public String updateOneVideo(HttpServletRequest request, Model model, @PathVariable("idVideo") Integer id, @RequestParam("miniature") MultipartFile miniature, @ModelAttribute("video") Video video) {
        //Video video1= videoService.findById(id);
        //videoService.update(video);
        //todo trouver miniature de la video et la remplacer par la nouvelle miniature
        Image minAncien = imageService.miniatureVideo(id);
        if (minAncien != null) {
            minAncien.setMiniature(false);
            imageService.update(minAncien);
        }
        //todo remplacer miniatureByte par la nouvelle
        Image min = imageService.uploadImage(miniature);

        min.setMiniature(true);
        video.getImages().add(min);

        min.setVideo(video);
        imageService.update(min);

        byte[] minByte = imageService.showMiniatureByIdVideo(video.getIdVideo());
        video.setByteMiniature(minByte);
        videoService.update(video);


        //request.getSession().setAttribute("miniature", minByte);
        model.addAttribute("video", video);
        return "jsp/detailsvideo";
    }


    @RequestMapping(value = "/admin/deleteVideo/{idVideo}", method = RequestMethod.GET)
    public ModelAndView deleteOneVideo(HttpServletRequest request, @PathVariable("idVideo") Integer id, Model model) {
        Video video = videoService.findById(id);
        String name = video.getTitre();

        videoService.deleteVideoIntoBucket(name);
        Image image=imageService.miniatureVideo(id);
        imageService.delete(image);

        List<Critique> critiques=critiqueService.getCritiquesByVideo(id);
        if(critiques.size()>0) {
            for (Critique critique : critiques
            ) {
                critiqueService.delete(critique);
            }
        }
        videoService.delete(video);
        List<Video> listVideos = videoService.findAll();
        listVideos = videoService.uploadMiniature(listVideos);

        ModelAndView mav = new ModelAndView("jsp/modifierVideos"); // créez un objet ModelAndView avec la vue JSP nommée "video"
        mav.addObject("videoData", listVideos); // ajoutez les données de la vidéo à l'objet ModelAndView

        return mav; // retournez l'objet ModelAndView à la vue JSP
    }
}


