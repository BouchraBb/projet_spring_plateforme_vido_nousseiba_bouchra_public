package fr.afpa.plateformevideo.controller;

import fr.afpa.plateformevideo.dto.UtilisateurDTO;
import fr.afpa.plateformevideo.entity.Historique;
import fr.afpa.plateformevideo.service.HistoriqueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class HistoriqueController {
    @Autowired
    HistoriqueService historiqueService;
    @RequestMapping("/showHistoric")
    public String showHistoric(HttpServletRequest request) {
        List<Historique> historicList = historiqueService.userHistorique(((UtilisateurDTO)request.getSession().getAttribute("utilisateur")).getIdUtilisateur());
        request.getSession().setAttribute("historicList", historicList);
        return "jsp/historique";
    }
}
