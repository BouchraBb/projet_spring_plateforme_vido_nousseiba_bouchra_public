package fr.afpa.plateformevideo.controller;

import com.google.cloud.storage.*;
import fr.afpa.plateformevideo.service.ImageService;
import fr.afpa.plateformevideo.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.plateformevideo.utils.*;

@Controller
public class ImageController {

    @Autowired
    ImageService imageService;
    @Autowired
    VideoService videoService;

    private Storage storage;
    String bucketName = Constantes.BUCKETNAME;


    public ImageController() {
    }


    @RequestMapping(value = "/upload/image", method = RequestMethod.GET)
    public String uploadImage() {
        return "jsp/saveImage";
    }

    @PostMapping("/upload/image")
    public String uploadVideo(@RequestParam("file") MultipartFile file) {
        imageService.uploadImage(file);
        return "jsp/saveImage";
    }


    @RequestMapping(value = "/image/show/all", method = RequestMethod.GET)
    public ModelAndView showAll() throws Exception {
        byte[][] images=imageService.showAllImages();

        ModelAndView mav = new ModelAndView("jsp/images"); // créez un objet ModelAndView avec la vue JSP nommée "video"
        mav.addObject("imageData", images); // ajoutez les données de la vidéo à l'objet ModelAndView
        return mav; // retournez l'objet ModelAndView à la vue JSP
    }


/*    @RequestMapping(value = "/image/show/allImagesByVideo/{idVideo}", method = RequestMethod.GET)
    public ModelAndView showAllImagesByVideo(@PathVariable ("idVideo") Integer id) throws Exception {
        byte[][] images=imageService.showAllImagesByVideo(id);
        ModelAndView mav = new ModelAndView("jsp/detailsvideo");
        mav.addObject("imageData2", images);
        return mav;
    }*/





}
