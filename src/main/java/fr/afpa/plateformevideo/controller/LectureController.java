package fr.afpa.plateformevideo.controller;

import fr.afpa.plateformevideo.dto.UtilisateurDTO;
import fr.afpa.plateformevideo.entity.Lecture;
import fr.afpa.plateformevideo.service.LectureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;
import java.util.Map;

@RestController
public class LectureController {
    @Autowired
    LectureService lectureService;


    @ResponseBody
    @PostMapping("/saveVideoTime")
    public void saveVideoTime(@RequestBody Map<String, Integer> requestMap, HttpServletRequest request) {
       // TODO : à corriger

        System.err.println("***************************************DEBUG: SAVE VIDEO TIME *******************************************************");
        Integer id = requestMap.get("id");
        Integer currentT = requestMap.get("currentT");
        System.err.println("getVideoTime : " + id + "currentT: "+ currentT);
        Integer idUser= ((UtilisateurDTO)request.getSession().getAttribute("utilisateur")).getIdUtilisateur();
        Lecture existingLecture = lectureService.userVideoLecture(idUser,id);
        existingLecture.setTimePause(currentT);
        lectureService.update(existingLecture);

    }
}
