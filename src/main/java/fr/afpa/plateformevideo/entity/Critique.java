package fr.afpa.plateformevideo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@NamedQueries({@NamedQuery(name = "Critique.critiquesByVideo", query = "select a from Critique a where a.video.idVideo = ?1")

})

public class Critique {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idCritique;
    private String commentaire;
    private Date dateCritique;
    private Integer score;


    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idUtilisateur")
    Utilisateur utilisateur;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idVideo")
    Video video;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idSerie")
    Serie serie;


    public Critique(String commentaire, Date dateCritique, Integer score, Utilisateur utilisateur, Video video) {
        this.commentaire = commentaire;
        this.dateCritique = dateCritique;
        this.score = score;
        this.utilisateur = utilisateur;
        this.video = video;
    }

    public static String scoreToStars(int score) {
        if (score < 1 || score > 5) {
            return "";
        }
        return "*".repeat(score);

    }

    @Override
    public String toString() {
        return "Critique{" +
                "idCritique=" + idCritique +
                ", commentaire='" + commentaire + '\'' +
                ", dateCritique=" + dateCritique +
                ", score=" + score +
                ", utilisateur=" + utilisateur.getIdUtilisateur() +
                ", video=" + video.getIdVideo() +
                '}';
    }
}
