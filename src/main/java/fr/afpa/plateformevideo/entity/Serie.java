package fr.afpa.plateformevideo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Serie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idSerie;
    private String titre;
    private Integer episode;
    private Integer saison;

    @OneToMany(mappedBy = "serie")
    private List<Video> episodes =new ArrayList<>();

    @OneToMany(mappedBy = "serie")
    private List<Critique> critiques =new ArrayList<>();




}
