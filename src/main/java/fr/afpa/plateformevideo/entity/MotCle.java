package fr.afpa.plateformevideo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MotCle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idMotCle;
    private String libelle;

    @ManyToMany(cascade={CascadeType.PERSIST} , mappedBy = "motCles")
    private List<Video> videos;


    @Override
    public String toString() {
        return "MotCle{" +
                "idMotCle=" + idMotCle +
                ", libelle='" + libelle + '\'' +
                '}';
    }
}