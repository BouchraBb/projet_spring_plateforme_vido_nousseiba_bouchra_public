package fr.afpa.plateformevideo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@NamedQuery(name = "Historique.userHistorique", query = "select h from Historique h where h.utilisateur.idUtilisateur = ?1 ")
public class Historique {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idHistorique;
    private Timestamp dateHistorique;
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idVideo")
    Video video;

    @ManyToOne
    @JoinColumn(name = "idUtilisateur")
    Utilisateur utilisateur;


    public Historique(Timestamp dateHistorique, Video video, Utilisateur utilisateur) {
        this.dateHistorique = dateHistorique;
        this.video = video;
        this.utilisateur = utilisateur;
    }

    @Override
    public String toString() {
        return "Historique{" +
                "idHistorique=" + idHistorique +
                ", dateHistorique=" + dateHistorique +
                '}';
    }
}
