package fr.afpa.plateformevideo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.mapping.IdGenerator;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@NamedQueries({ @NamedQuery(name="Video.videosSimilaires", query="SELECT DISTINCT v \n" +
        "FROM Video v \n" +
        "JOIN v.motCles m \n" +
        "WHERE m IN (SELECT mc FROM Video v2 JOIN v2.motCles mc WHERE v2.idVideo = ?1) \n" +
        "AND v.idVideo != ?1"),
        @NamedQuery(name = "Video.findFavoritesByUserId",
        query = "SELECT v FROM Video v JOIN v.utilisateurs u WHERE u.idUtilisateur = :userId")})


public class Video{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idVideo;
    private String titre;
    private Integer duree;
    private String genre;
    private String url;
    private Integer numSaison;
    private Integer numEpisode;
    private String description;
    private byte[] byteMiniature;
    private Integer vues=0;

    private Date annee;


    @OneToMany(mappedBy = "video")
    private List<Historique> historiques =new ArrayList<>();

    @OneToMany(mappedBy = "video")
    private List<Lecture> lectures =new ArrayList<>();

    @OneToMany(mappedBy = "video")
    private List<Critique> critiques =new ArrayList<>();

    @ManyToMany (cascade={CascadeType.PERSIST} , mappedBy = "videos")
    private List<Utilisateur> utilisateurs;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idSerie")
    Serie serie;

    @OneToMany(mappedBy = "video")
    private List<Image> images =new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "listeMotCle")
    List<MotCle> motCles;

    @ManyToMany(cascade={CascadeType.PERSIST} , mappedBy = "videos")
    private List<Acteur> acteurs;


    public Video(String titre, Integer duree, String genre, String description, Date annee) {
        this.titre = titre;
        this.duree = duree;
        this.genre = genre;
        this.description = description;
        this.annee = annee;
    }

    public void ajouterUneVue(){
        if (vues==null){
            vues=0;
        }
        vues+=1;
    }


}
