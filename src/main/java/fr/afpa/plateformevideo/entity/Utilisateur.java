package fr.afpa.plateformevideo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@NamedQueries({@NamedQuery(name = "Utilisateur.findByEmailAndPassword",
        query = "select u from Utilisateur u where u.email = ?1 and u.motDePasse= ?2"),
        @NamedQuery(name="Video.favoritsVideo", query= "SELECT u FROM Utilisateur u JOIN u.videos v WHERE u.idUtilisateur = :idUtilisateur")})

public class Utilisateur {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idUtilisateur;
    private String email;
    private String motDePasse;
    private String nom;
    private String prenom;
    private String pseudo;

   @OneToMany(mappedBy = "utilisateur")
    private List<Critique> critiques =new ArrayList<>();


    @OneToMany(mappedBy = "utilisateur")
    private List<Abonnement> abonnements =new ArrayList<>();

    @OneToMany(mappedBy = "utilisateur")
    private List<Historique> historiques =new ArrayList<>();
    @OneToMany(mappedBy = "utilisateur")
    private List<Lecture> lectures =new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "favorie", uniqueConstraints = @UniqueConstraint(columnNames = {"utilisateurs_id_utilisateur", "videos_id_video"}))
    List<Video> videos=new ArrayList<>();


    public Utilisateur(String email, String motDePasse, String nom, String prenom, String pseudo) {
        this.email = email;
        this.motDePasse = motDePasse;
        this.nom = nom;
        this.prenom = prenom;
        this.pseudo = pseudo;
    }

    @Override
    public String toString() {
        return "UtilisateurDTO{" +
                "email='" + email + '\'' +
                ", prenom='" + prenom + '\'' +
                ", pseudo='" + pseudo + '\'' +
                '}';
    }
}
