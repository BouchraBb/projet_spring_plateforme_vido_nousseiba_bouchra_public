package fr.afpa.plateformevideo.dto;

import fr.afpa.plateformevideo.entity.Abonnement;
import fr.afpa.plateformevideo.entity.Critique;
import fr.afpa.plateformevideo.entity.Historique;
import fr.afpa.plateformevideo.entity.Video;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.UniqueConstraint;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UtilisateurDTO {
    private Integer idUtilisateur;
    private String email;
    private String pseudo ;
    private List<Critique> critiques =new ArrayList<>();
    private List<Abonnement> abonnements =new ArrayList<>();
    private List<Historique> historiques =new ArrayList<>();
    private List<Video> videos =new ArrayList<>();

    @Override
    public String toString() {
        return "UtilisateurDTO{" +
                "email='" + email + '\'' +
                ", pseudo='" + pseudo + '\'' +
                '}';
    }
}
