package fr.afpa.plateformevideo.service;

import fr.afpa.plateformevideo.entity.Admin;

import java.util.List;

public interface AdminService {

    public Admin create(Admin Admin);
    public List<Admin> findAll();

    public Admin findById(Integer id);

    public void update(Admin Admin);

    public void delete(Admin Admin);
}
