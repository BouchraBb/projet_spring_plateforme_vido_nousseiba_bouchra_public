package fr.afpa.plateformevideo.service;

import fr.afpa.plateformevideo.entity.Abonnement;

import java.util.List;

public interface AbonnementService {

    public Abonnement create(Abonnement Abonnement);
    public List<Abonnement> findAll();

    public Abonnement findById(Integer id);

    public void update(Abonnement Abonnement);

    public void delete(Abonnement Abonnement);
}
