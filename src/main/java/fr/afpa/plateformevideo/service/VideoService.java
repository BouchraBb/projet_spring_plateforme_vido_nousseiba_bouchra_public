package fr.afpa.plateformevideo.service;

import fr.afpa.plateformevideo.entity.Video;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface VideoService {

    public Video create(Video video);
    public List<Video> findAll();

    public Video findById(Integer id);

    public void update(Video video);

    public void delete(Video video);

    byte[] showOneVideo(Video video);

    List<Video> videosSimilaires(int idVideo);

    byte[][] showVideos(List<Video> videoList);

    List<Video> uploadMiniature(List<Video> all);

    String uploadVideoIntoBucket(MultipartFile file);

    void deleteVideoIntoBucket(String fileName);
}
