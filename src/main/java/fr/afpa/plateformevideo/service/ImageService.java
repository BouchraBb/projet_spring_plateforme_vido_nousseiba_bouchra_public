package fr.afpa.plateformevideo.service;

import fr.afpa.plateformevideo.entity.Image;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ImageService {
    public Image create(Image image);
    public List<Image> findAll();

    public Image findById(Integer id);

    public void update(Image image);

    public void delete(Image image);



    byte[][] showAllImagesByVideo(Integer id);

    byte[][] showAllImages();

    Image uploadImage(MultipartFile file);

    Image miniatureVideo(Integer id);

    byte[] showOneImage(Image image);

    byte[] showMiniatureByIdVideo(Integer id);
}
