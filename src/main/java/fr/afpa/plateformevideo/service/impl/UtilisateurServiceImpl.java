package fr.afpa.plateformevideo.service.impl;

import fr.afpa.plateformevideo.dto.UtilisateurDTO;
import fr.afpa.plateformevideo.entity.Historique;
import fr.afpa.plateformevideo.entity.Utilisateur;
import fr.afpa.plateformevideo.entity.Video;
import fr.afpa.plateformevideo.repository.UtilisateurRepository;
import fr.afpa.plateformevideo.repository.VideoRepository;
import fr.afpa.plateformevideo.service.HistoriqueService;
import fr.afpa.plateformevideo.service.UtilisateurService;
import fr.afpa.plateformevideo.service.VideoService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

@Service
public class UtilisateurServiceImpl implements UtilisateurService {
    ModelMapper modelMapper = new ModelMapper();
    @Autowired
    UtilisateurRepository UtilisateurRepository;
     @Autowired
    VideoRepository videoRepository;
    @Autowired
    VideoService videoService;

    @Autowired
    HistoriqueService historiqueService;

    @Override
    public UtilisateurDTO create(Utilisateur utilisateur) {
        return modelMapper.map( UtilisateurRepository.save(utilisateur),UtilisateurDTO.class) ;
    }

    @Override
    public List<Utilisateur> findAll() {
        return UtilisateurRepository.findAll();
    }

    @Override
    public UtilisateurDTO findById(Integer id) {
        return  modelMapper.map(UtilisateurRepository.findById(id).orElse(null), UtilisateurDTO.class);
    }

    @Override
    public Utilisateur findById1(Integer id) {
        return  UtilisateurRepository.findById(id).orElse(null);
    }

    @Override
    public void update(UtilisateurDTO utilisateur) {
        UtilisateurRepository.saveAndFlush( modelMapper.map(utilisateur, Utilisateur.class));
    }

    private void update1(Utilisateur utilisateur) {
        UtilisateurRepository.saveAndFlush( utilisateur);
    }

    @Override
    public void delete(Utilisateur utilisateur) {
        UtilisateurRepository.delete(utilisateur);
    }

    @Override
    public UtilisateurDTO authentificate(String email, String password){
        System.out.println(email);
        System.out.println(password);
        Utilisateur utilisateur = UtilisateurRepository.findByEmailAndPassword(email, password);
        if(utilisateur!=null){
            return modelMapper.map(utilisateur, UtilisateurDTO.class) ;
        }
        else return null;
    }

    @Override
    public void addVideoToHistorique(UtilisateurDTO user, Integer id) {
        Video video = videoService.findById(id);
        Utilisateur utilisateur  = findById1(user.getIdUtilisateur());

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
        java.sql.Timestamp date = new java.sql.Timestamp(calendar.getTime().getTime());

        Historique historique= new Historique(date, video, utilisateur );
        historiqueService.create(historique);
        utilisateur.getHistoriques().add(historique);
        update1(utilisateur);
    }

    @Override
    public void addVideoToFavorit(UtilisateurDTO user,  Integer id) {
        Video video = videoService.findById(id);
        Utilisateur utilisateur = findById1(user.getIdUtilisateur());
        utilisateur.getVideos().add(video);
        update1(utilisateur);
    }

    @Override
    public void removeVideoFromFavorit(UtilisateurDTO user, Integer id) {
        Video video = videoService.findById(id);
        Utilisateur utilisateur = findById1(user.getIdUtilisateur());
        utilisateur.getVideos().remove(video);
        update1(utilisateur);
    }

    @Override
    public List<Video> userFavorit(Integer idUtilisateur) {
        return videoRepository.findFavoritesByUserId(idUtilisateur);
    }



}
