package fr.afpa.plateformevideo.service.impl;

import fr.afpa.plateformevideo.entity.Serie;
import fr.afpa.plateformevideo.repository.SerieRepository;
import fr.afpa.plateformevideo.service.SerieService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class SerieServiceImpl implements SerieService {
    @Autowired
    SerieRepository serieRepository;

    @Override
    public Serie create(Serie serie) {
        return serieRepository.save(serie);
    }

    @Override
    public List<Serie> findAll() {
        return serieRepository.findAll();
    }

    @Override
    public Serie findById(Integer id) {
        return serieRepository.findById(id).orElse(null);
    }

    @Override
    public void update(Serie serie) {
        serieRepository.saveAndFlush(serie);
    }

    @Override
    public void delete(Serie serie) {
        serieRepository.delete(serie);
    }
}
