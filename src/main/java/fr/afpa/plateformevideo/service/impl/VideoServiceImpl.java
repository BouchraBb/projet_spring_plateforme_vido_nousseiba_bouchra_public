package fr.afpa.plateformevideo.service.impl;

import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import fr.afpa.plateformevideo.entity.Video;
import fr.afpa.plateformevideo.repository.VideoRepository;
import fr.afpa.plateformevideo.service.ImageService;
import fr.afpa.plateformevideo.service.VideoService;
import fr.afpa.plateformevideo.utils.Constantes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.List;

@Service
public class VideoServiceImpl implements VideoService {
    @Autowired
    VideoRepository VideoRepository;
    @Autowired
    ImageService imageService;

    String bucketName = Constantes.BUCKETNAME;

    Storage storage= ImageServiceImpl.storage;


    @Override
    public Video create(Video video) {
        return VideoRepository.save(video);
    }

    @Override
    public List<Video> findAll() {
        return VideoRepository.findAll();
    }

    @Override
    public Video findById(Integer id) {
        return VideoRepository.findById(id).orElse(null);
    }

    @Override
    public void update(Video video) {
        VideoRepository.saveAndFlush(video);
    }

    @Override
    public void delete(Video video) {
        VideoRepository.delete(video);
    }


    @Override
    public byte[] showOneVideo(Video video) {
        //byte[] images = new byte[];
        BlobId blobId = BlobId.of(bucketName,video.getTitre()); // remplacez my-bucket et video.mp4 par le nom de votre bucket et le nom de votre vidéo
        Blob blob = storage.get(blobId); // récupérez la vidéo à partir de GCP

        return blob.getContent();
    }

    @Override
    public List<Video> videosSimilaires(int idVideo) {
        return VideoRepository.videosSimilaires(idVideo);
    }


    @Override
    public byte[][] showVideos(List<Video> videoList) {

        byte[][] videos = new byte[videoList.size()][];
        int cpt = 0;
        for (Video video : videoList) {

            BlobId blobId = BlobId.of(bucketName, video.getTitre()); // remplacez my-bucket et video.mp4 par le nom de votre bucket et le nom de votre vidéo
            Blob blob = storage.get(blobId); // récupérez la vidéo à partir de GCP
            //videos[cpt] = new byte[blob.getContent().length];
            videos[cpt] = blob.getContent();
            cpt++;

        }
        return videos;
    }

    @Override
    public List<Video> uploadMiniature(List<Video> listVideos ) {
            byte[][] miniatures=new byte[listVideos.size()][];
            for (int i=0; i<listVideos.size();i++){
                miniatures[i]= imageService.showMiniatureByIdVideo(listVideos.get(i).getIdVideo());
                listVideos.get(i).setByteMiniature(miniatures[i]);
                update(listVideos.get(i));
            }
            return listVideos;
        }

    @Override
    public String uploadVideoIntoBucket(MultipartFile file) {
        String name=null;
        try {
            String fileName = file.getOriginalFilename();
            InputStream inputStream = file.getInputStream();
            BlobId blobId = BlobId.of(bucketName, fileName);
            BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("video/mp4").build();
            name =   storage.create(blobInfo, inputStream.readAllBytes()).getName();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return name;
    }

    @Override
    public void deleteVideoIntoBucket(String fileName) {
        try {
            BlobId blobId = BlobId.of(bucketName, fileName);
            storage.delete(blobId);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
