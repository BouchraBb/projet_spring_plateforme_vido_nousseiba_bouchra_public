package fr.afpa.plateformevideo.service.impl;

import fr.afpa.plateformevideo.entity.Admin;
import fr.afpa.plateformevideo.entity.Admin;
import fr.afpa.plateformevideo.repository.AdminRepository;
import fr.afpa.plateformevideo.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    AdminRepository AdminRepository;


    @Override
    public Admin create(Admin Admin) {
        return AdminRepository.save(Admin);

    }

    @Override
    public List<Admin> findAll() {
        return AdminRepository.findAll();
    }

    @Override
    public Admin findById(Integer id) {
        return AdminRepository.findById(id).orElse(null);
    }

    @Override
    public void update(Admin admin) {
        AdminRepository.saveAndFlush(admin);
    }

    @Override
    public void delete(Admin admin) {
        AdminRepository.delete(admin);
    }
}
