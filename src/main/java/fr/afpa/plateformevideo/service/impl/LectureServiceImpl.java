package fr.afpa.plateformevideo.service.impl;

import fr.afpa.plateformevideo.dto.UtilisateurDTO;
import fr.afpa.plateformevideo.entity.Lecture;
import fr.afpa.plateformevideo.entity.Utilisateur;
import fr.afpa.plateformevideo.entity.Video;
import fr.afpa.plateformevideo.repository.LectureRepository;
import fr.afpa.plateformevideo.service.LectureService;
import fr.afpa.plateformevideo.service.UtilisateurService;
import fr.afpa.plateformevideo.service.VideoService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class LectureServiceImpl implements LectureService {
    ModelMapper modelMapper = new ModelMapper();
    @Autowired
    LectureRepository lectureRepository;
    @Autowired
    VideoService videoService;
    @Override
    public Lecture create(Lecture lecture) {
        return lectureRepository.save(lecture);
    }

    @Override
    public List<Lecture> findAll() {
        return lectureRepository.findAll();
    }

    @Override
    public Lecture findById(Integer id) {
        return lectureRepository.findById(id).orElse(null);
    }

    @Override
    public void update(Lecture lecture) {
        lectureRepository.saveAndFlush(lecture);

    }

    @Override
    public void delete(Lecture lecture) {
        lectureRepository.delete(lecture);

    }

    @Override
    public void addVideoToLecture(UtilisateurDTO user, Integer id) {
        Video video = videoService.findById(id);
        Lecture lecture =null;
         lecture = userVideoLecture(user.getIdUtilisateur(), id);
         if(lecture==null) {
              lecture = new Lecture(video, modelMapper.map(user, Utilisateur.class));
             create(lecture);
         }
    }

    @Override
    public List<Lecture> allUserVideoLecture(Integer idUser) {
        return lectureRepository.allUserVideoLecture(idUser);
    }

    @Override
    public Lecture userVideoLecture(Integer idUser, Integer idVideo) {
        return lectureRepository.userVideoLecture(idUser,idVideo);
    }
}