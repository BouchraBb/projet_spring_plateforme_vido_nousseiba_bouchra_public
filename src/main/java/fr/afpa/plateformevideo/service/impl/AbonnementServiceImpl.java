package fr.afpa.plateformevideo.service.impl;

import fr.afpa.plateformevideo.entity.Abonnement;
import fr.afpa.plateformevideo.repository.AbonnementRepository;
import fr.afpa.plateformevideo.service.AbonnementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AbonnementServiceImpl implements AbonnementService {

    @Autowired
    AbonnementRepository abonnementRepository;


    @Override
    public Abonnement create(Abonnement abonnement) {
        return abonnementRepository.save(abonnement);

    }

    @Override
    public List<Abonnement> findAll() {
        return abonnementRepository.findAll();
    }

    @Override
    public Abonnement findById(Integer id) {
        return abonnementRepository.findById(id).orElse(null);
    }

    @Override
    public void update(Abonnement abonnement) {
        abonnementRepository.saveAndFlush(abonnement);
    }

    @Override
    public void delete(Abonnement abonnement) {
        abonnementRepository.delete(abonnement);
    }
}
