package fr.afpa.plateformevideo.service.impl;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.*;
import fr.afpa.plateformevideo.entity.Image;
import fr.afpa.plateformevideo.entity.Video;
import fr.afpa.plateformevideo.repository.ImageRepository;
import fr.afpa.plateformevideo.service.ImageService;
import fr.afpa.plateformevideo.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import fr.afpa.plateformevideo.utils.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Service
public class ImageServiceImpl implements ImageService {
    @Autowired
    ImageRepository imageRepository;

    @Autowired
    VideoService videoService;

    String bucketName = Constantes.BUCKETNAME;

    static Storage storage= storageCloud();

    @Override
    public Image create(Image image) {
        return imageRepository.save(image);
    }

    @Override
    public List<Image> findAll() {
        return imageRepository.findAll();
    }

    @Override
    public Image findById(Integer id) {
        return imageRepository.findById(id).orElse(null);
    }

    @Override
    public void update(Image image) {
        imageRepository.saveAndFlush(image);
    }

    @Override
    public void delete(Image image) {
        imageRepository.delete(image);

    }


    public static Storage storageCloud(){
        storage = StorageOptions.getDefaultInstance().getService();

        try {
            storage = StorageOptions.newBuilder()
                    .setCredentials(GoogleCredentials
                            .fromStream(new FileInputStream("src\\files\\buoyant-episode-384706-0a165e9eb785.json")))
                    .build()
                    .getService();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return storage;
    }

    @Override
    public byte[][] showAllImagesByVideo(Integer id) {
        Video video = videoService.findById(id);
        List<Image> imageList = video.getImages();

        byte[][] images = new byte[imageList.size()][];
        int cpt = 0;
        for (Image image : imageList) {

            BlobId blobId = BlobId.of(bucketName, image.getNom()); // remplacez my-bucket et video.mp4 par le nom de votre bucket et le nom de votre vidéo
            Blob blob = storage.get(blobId); // récupérez la vidéo à partir de GCP
            //videos[cpt] = new byte[blob.getContent().length];
            images[cpt] = blob.getContent();
            System.out.println(blob.getContent());
            cpt++;

        }
        return images;
    }


    @Override
    public byte[][] showAllImages() {
        List<Image> imageList = findAll();

        byte[][] images = new byte[imageList.size()][];

        int cpt=0;
        for (Image image:imageList ) {
            System.out.println(image.getIdImage());
            System.out.println(bucketName);

            BlobId blobId = BlobId.of(bucketName,image.getNom()); // remplacez my-bucket et video.mp4 par le nom de votre bucket et le nom de votre vidéo
            //System.out.println(blobId);
            Blob blob = storage.get(blobId); // récupérez la vidéo à partir de GCP
            //videos[cpt] = new byte[blob.getContent().length];
            images[cpt] = blob.getContent();
            System.out.println(blob.getContent());
            cpt++;

        }
        return images;
    }

    @Override
    public Image uploadImage(MultipartFile file) {
        Image image =null;
        try {
            String fileName = file.getOriginalFilename();
            InputStream inputStream = file.getInputStream();
            BlobId blobId = BlobId.of(bucketName, fileName);
            BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("image/png").build();
            String name = storage.create(blobInfo, inputStream.readAllBytes()).getName();
            image = new Image();
            image.setNom(name);
            create(image);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return image;
    }

    @Override
    public Image miniatureVideo(Integer id){
        return imageRepository.miniatureVideo(id);
    }



    @Override
    public byte[] showOneImage(Image image) {
          //byte[] images = new byte[];
            BlobId blobId = BlobId.of(bucketName,image.getNom()); // remplacez my-bucket et video.mp4 par le nom de votre bucket et le nom de votre vidéo
            Blob blob = storage.get(blobId); // récupérez la vidéo à partir de GCP

        return blob.getContent();
    }


    @Override
    public byte[] showMiniatureByIdVideo(Integer id) {
        Image image = miniatureVideo(id);
        byte[] miniature=null;
        if (image != null) {
            miniature = showOneImage(image);

        }
        return miniature;
    }

}
