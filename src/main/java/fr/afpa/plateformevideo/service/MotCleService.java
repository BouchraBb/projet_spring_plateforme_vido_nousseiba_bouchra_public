package fr.afpa.plateformevideo.service;

import fr.afpa.plateformevideo.entity.MotCle;

import java.util.List;

public interface MotCleService {

    public MotCle create(MotCle motCle);
    public List<MotCle> findAll();

    public MotCle findById(Integer id);

    public void update(MotCle motCle);

    public void delete(MotCle motCle);
}
