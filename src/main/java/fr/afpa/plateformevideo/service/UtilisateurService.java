package fr.afpa.plateformevideo.service;
import fr.afpa.plateformevideo.dto.UtilisateurDTO;
import fr.afpa.plateformevideo.entity.Utilisateur;
import fr.afpa.plateformevideo.entity.Video;

import java.util.List;

public interface UtilisateurService {

    public UtilisateurDTO create(Utilisateur utilisateur);
    public List<Utilisateur> findAll();

    public UtilisateurDTO findById(Integer id);

    Utilisateur findById1(Integer id);

    public void update(UtilisateurDTO utilisateur);

    public void delete(Utilisateur utilisateur);

    UtilisateurDTO authentificate(String email, String password);

    void addVideoToHistorique(UtilisateurDTO user, Integer id);

    void addVideoToFavorit(UtilisateurDTO user,  Integer id);

    void removeVideoFromFavorit(UtilisateurDTO user, Integer id);

    List<Video> userFavorit(Integer utilisateur);


}
