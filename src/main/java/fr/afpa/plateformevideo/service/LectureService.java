package fr.afpa.plateformevideo.service;

import fr.afpa.plateformevideo.dto.UtilisateurDTO;
import fr.afpa.plateformevideo.entity.Lecture;
import fr.afpa.plateformevideo.entity.MotCle;

import java.util.List;

public interface LectureService {
    public Lecture create(Lecture lecture);
    public List<Lecture> findAll();

    public Lecture findById(Integer id);

    public void update(Lecture lecture);

    public void delete(Lecture lecture);
    void addVideoToLecture(UtilisateurDTO user, Integer id);
    public List<Lecture> allUserVideoLecture(Integer idUser);
    public Lecture userVideoLecture(Integer idUser, Integer idVideo);
}
